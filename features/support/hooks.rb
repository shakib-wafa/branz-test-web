# @browser.start_browser
#
# Before do
#   #$driver.start_driver
# end
#
# After do |scenario|
#   if scenario.failed?
#     if !File.directory?("screenshots")
#       FileUtils.mkdir("screenshots")
#     end
#     time_stamp = Time.now.strftime("%Y-%m-%d %H.%M.%S")
#     screenshot_name = time_stamp + ".png"
#     screenshot_file = File.join("screenshots", screenshot_name)
#     @browser.screenshot(screenshot_file)
#     embed("#{screenshot_file}", "image/png")
#   end
#   #$driver.driver_quit
# end
#
# at_exit do
#   exec "node runreport.js"
#   @browser.browser_quit
# end
#
# AfterConfiguration do
#   FileUtils.rm_r("screenshots") if File.directory?("screenshots")
# end
