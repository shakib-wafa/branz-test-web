@InitiateNewProject
Feature: Initiate New Project
  Scenario: 2.1 Initiate New Project as a Consenter
  Given User selects INITIATE NEW PROJECT button
  When 2.1.1 Set Project Information
  #    Then 2.1.2 Select shotlists
  Then 2.1.3 Flag Shotlists requiring physical Inspection
  Then 2.1.4 Select Applicable Shotlist Tasks
  And 2.1.5 Record Primary Applicant