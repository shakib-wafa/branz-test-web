require 'selenium-webdriver'
require 'cucumber'
require 'watir'
require 'securerandom'


#Weblogic
Given("User successfully logs in to the Artisan Web page") do
  @LoginPage = LoginPage.new(@browser)
  @LoginPage.visit
  @LoginPage.enterUsername("consenter+akl@alphero.com")
  @LoginPage.enterPassword("Password1")
  @LoginPage.clickLoginButton()
  @browser.window.maximize
  sleep(5)
  @browser.close
end

#**************************************************************************************************************

#Initiate New Project
Given("User selects INITIATE NEW PROJECT button") do
  @LoginPage = LoginPage.new(@browser)
  @LoginPage.visit
  @LoginPage.enterUsername("consenter+akl@alphero.com")
  @LoginPage.enterPassword("Password1")
  @LoginPage.clickLoginButton()
  @browser.window.maximize
  @browser.button(:id, 'ember707').click
end

number = rand.to_s[2..10]
projectId = 'Autotest'+ number
When(/^(\d+)\.(\d+)\.(\d+) Set Project Information$/) do |arg1, arg2, arg3|
  @browser.text_field(:id, 'consentNumber-ember847').set projectId
  @browser.text_field(:id, 'propertyAddress-ember852').set '1 Lane street, upper hutt'
  @browser.button(:id, 'ember861').click
end

# Then(/^(\d+)\.(\d+)\.(\d+) Select shotlists$/) do |arg1, arg2, arg3|
#   @browser.text(:class, 'PageControls-context-title').highlight
#   @browser.element(:class, 'h3 u-textBlue u-textRegular ').click
#
#   # @browser.checkbox(:class,'Checkbox-label').click
#   # sleep(3)
#   # @browser.button(:class, 'ic ic-chevron-left').click
# end


Then(/^(\d+)\.(\d+)\.(\d+) Flag Shotlists requiring physical Inspection$/) do |arg1, arg2, arg3|
  # @browser.button(:class,'Button Button--nude').text_field('ADD PHYSICAL INSPECTIONS').click
  @browser.element(:text, 'ADD PHYSICAL INSPECTIONS').click
  @browser.element(:class, 'Checkbox-label').click
  sleep(2)
  @browser.element(:text, 'DONE ADDING PHYSICAL INSPECTIONS').click
  sleep(5)
  @browser.screenshot.save 'PhysicalInspectionPage.png'
end

Then(/^(\d+)\.(\d+)\.(\d+) Select Applicable Shotlist Tasks$/) do |arg1, arg2, arg3|
  @browser.element(:class, 'td--draggable u-pR-0').click
  @browser.button(:id, 'ember1076').click
end

And(/^(\d+)\.(\d+)\.(\d+) Record Primary Applicant$/) do |arg1, arg2, arg3|
  @browser.text_field(:id, 'firstName-ember1123').set 'Autotest'
  @browser.text_field(:id, 'lastName-ember1145').set '123'
  @browser.text_field(:id, 'email-ember1150').set 'autotest123@gmail.com'
  sleep(5)
  @browser.screenshot.save 'RecordPrimaryApplicantPage.png'
  @browser.button(:id, 'ember1154').click
  p = @browser.element(:class, 'Result-title')
  sleep(5)
  #   puts "Project initiation is successful"
  if p != "true"
    @browser.screenshot.save 'TestResultsPage.png'
    puts "Test Passed"
    puts "ProjectID: " +projectId
  else
    puts "Test failed"
  end
  @browser.button(:id, 'ember1210').click
  @browser.quit
end

#****************************************************************************************************************